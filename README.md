# Baikal - CalDAV and CardDAV server with integrated CloudInf

This dockerfile provides a ready-to-go [Baikal server](http://sabre.io/baikal/) with
integrated [InfCloud web client](https://www.inf-it.com/open-source/clients/infcloud/).

This image uses the minimal alpine 3.5 with nginx.


## Features

  * Alpine Linux
  * nginx
  * PHP5
  * InfCloud web client integrated


## What is Baikal?

From <http://sabre.io/baikal/>:

Baikal is a Cal and CardDAV server, based on sabre/dav, that includes an administrative interface for easy management.

For more information, read the main website at baikal-server.com.

Baikal is developed by Net Gusto and fruux.


## What is InfCloud?

From <https://www.inf-it.com/open-source/clients/infcloud/>:

InfCloud is an open source CalDAV/CardDAV web client implementation released under
GNU Affero General Public License (version 3.0).

InfCloud is the integrated version of CalDavZAP and CardDavMATE, and includes all features of both clients.


## How to use this image?

The following command will start Baikal:

```bash
docker run --rm -p 80:80 \
    -v /baikal-data:/var/www/baikal/Specific \
    -v /baikal-data/infcloud-config:/var/www/infcloud/config \
    plaguedr/baikal:latest
```

Alternatively, use the docker-compose:

```yaml
# baikal with infcloud web client
---

version: '2'

services:
  baikal:
  image: plaguedr/baikal:latest
    expose:
      - "80:80/tcp"
    volumes:
      - /baikal-data:/var/www/baikal/Specific
      - /baikal-data/infcloud-config:/var/www/infcloud/config
    restart: always
```


## Configuration and Administration


### Baikal

Setup and administration of the Baikal server is done through [http://localhost/admin](http://localhost/admin) web gui.


### InfCloud

Check and modify file `/baikal-data/infcloud-config/config.js` to your needs.

You can access InfCloud interface through [http://localhost/calendar](http://localhost/calendar) url.


#### Configuration changes and fixes

Configuration differs from the default one:

```
var globalNetworkCheckSettings={
        href: 'http://localhost/cal.php/principals/',
        timeOut: 90000,
        lockTimeOut: 10000,
        checkContentType: true,
        settingsAccount: true,
        delegation: true,
        additionalResources: [],
        hrefLabel: null,
        forceReadOnly: null,
        ignoreAlarms: false,
        backgroundCalendars: []
}

var globalUseJqueryAuth=true;

var globalTimeZone='Australia/Melbourne';

var globalAddressCountryEquivalence=[
    {country: 'au', regex: '^\\W*Australia\\W*$'},
    {country: 'pl', regex: '^\\W*Polska\\W*$'}
];

var globalAddressCountryFavorites=['au', 'pl'];
```

I have also fixed `globalSortAlphabet` and `globalSearchTransformAlphabet` variables,
so the CloudInf can correctly sort and search with Polish alphabet.

Do not forget to restart the container after any modifications done to the CloudInf config file.


## Known issues and TODO

- this build doesn't work with alpine >3.5


## Source

The source of the project is located on GitLab: [docker-baikal](https://gitlab.com/Plague_Doctor/docker-baikal)

## See also

https://github.com/Cyconet/docker-infcloud
https://github.com/maschel/docker-baikal

## Donate

If you like this project, please donate. You can send coins to the following addresses.

**Bitcoin**: 3KWsKEw3Ewu7vcTREjQUuR8LUf4QXoZEHK

**Litecoin**: MHaqGAqMoQGiTkNyg7w8haC6mU25Frgi3M