FROM alpine:3.9
MAINTAINER Plague Doctor

ENV BAIKAL_VERSION 0.5.1
ENV BAIKAL_PKG baikal-$BAIKAL_VERSION.zip
# ENV BAIKAL_URL https://github.com/sabre-io/Baikal/archive/master.zip
# Temporarily moved to EpicCoder as there is an important update
ENV BAIKAL_URL https://github.com/EpicCoder/Baikal/archive/master.zip

ENV INFCLOUD_VERSION 0.13.1
ENV INFCLOUD_CHECKSUM 9fa95edd2dcc2b864a10b503ab9220895ea28d4c5541ab02117de1511d5464d4
ENV INFCLOUD_PKG InfCloud_$INFCLOUD_VERSION.zip
ENV INFCLOUD_URL https://www.inf-it.com/$INFCLOUD_PKG

ARG TZ=Etc/UTC
ENV TZ ${TZ}

EXPOSE 80
VOLUME /var/www/baikal/Specific
VOLUME /var/www/infcloud/config

ADD /root /

# Install important stuff
RUN apk --update --no-cache add \
        tzdata \
        nginx \
        php7-cgi \
        php7-ctype \
        php7-dom \
        php7-pdo_sqlite \
        php7-pdo_mysql \
        php7-xml \
        php7-openssl \
        php7-json \
        php7-xmlreader \
        php7-xmlwriter \
        php7-session \
        php7-mbstring \
        php7-mysqli \
        php7-mcrypt \
        php7-fpm \
        s6 \
    && apk add --no-cache --virtual .build-deps \
        alpine-sdk \
        php7-simplexml \
        php7-tokenizer \
        composer \
        make \
        openssl \
        unzip \
        zip \
        rsync \
# Install Baikal
    && cd /tmp \
    && wget $BAIKAL_URL -O /tmp/master.zip \
    && unzip master.zip \
    && cd /tmp/Baikal-master \
    && make build-assets \
    && make dist \
    && unzip /tmp/Baikal-master/build/$BAIKAL_PKG -d /var/www \
    && ls -la /tmp/Baikal-master/build/ \
# Install and configure InfCloud
    && sed -ie "s/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/g" /etc/php7/php.ini \
    && wget $INFCLOUD_URL \
    && echo $INFCLOUD_CHECKSUM "" $INFCLOUD_PKG | sha256sum -c - \
    && unzip $INFCLOUD_PKG -d /var/www \
    && rm $INFCLOUD_PKG \
    && mv /var/www/infcloud/config.js /var/www/infcloud/config.js.orig \
    && mv /var/www/infcloud/cache_update.sh /var/www/infcloud/cache_update.sh.orig \
    && ln -s /var/www/infcloud/config/config.js /var/www/infcloud/config.js \
    && ln -s /usr/local/bin/cache_update.sh /var/www/infcloud/cache_update.sh \
    && chmod +x /usr/local/bin/infcloud.sh /usr/local/bin/cache_update.sh \
    && sync; /var/www/infcloud/cache_update.sh \
    && chmod +x /entrypoint.sh \
# Setup local timezone
    && echo ${TZ} > /etc/timezone \
    && rm /etc/localtime \
    && ln -snf /usr/share/zoneinfo/${TZ} /etc/localtime \
# Cleanup
    && apk del .build-deps \
    && rm -rf \
        /var/lib/apt/lists/* \
        /tmp/* \
        /var/tmp/*

ENTRYPOINT ["/entrypoint.sh"]
