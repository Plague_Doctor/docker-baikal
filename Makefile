default: build-image

docker-image = plaguedr/baikal
docker-tag = 0.5.1-alpine-3.9
timezone = Australia/Melbourne

build-image:
	docker build \
		--build-arg TZ=$(timezone) \
		-t $(docker-image):$(docker-tag) \
		-t $(docker-image):latest \
		.

push-images:
	docker push $(docker-image):$(docker-tag)
	docker push $(docker-image):latest

run-baikal:
	docker run \
		-ti \
		-p 80:80 \
		-v /docker/baikal:/var/www/baikal/Specific \
		-v /docker/baikal/config:/var/www/infcloud/config \
		$(docker-image):$(docker-tag)
