#!/bin/sh

[ ! -f /var/www/infcloud/config/config.js ] && cp /var/www/infcloud/custom_config.js /var/www/infcloud/config/config.js

mkdir -p /var/www/baikal/Specific/db /var/tmp/nginx/client_body \
  && chown -R nobody:nobody /var/www/baikal/Specific \
  && exec s6-svscan /etc/s6/
